[Ivy]
178CFF78CCDA4305 7.5.0 #module
>Proto >Proto Collection #zClass
Ws0 WebServiceInformationProcess Big #zClass
Ws0 RD #cInfo
Ws0 #process
Ws0 @AnnotationInP-0n ai ai #zField
Ws0 @TextInP .type .type #zField
Ws0 @TextInP .processKind .processKind #zField
Ws0 @TextInP .xml .xml #zField
Ws0 @TextInP .responsibility .responsibility #zField
Ws0 @UdInit f0 '' #zField
Ws0 @UdProcessEnd f1 '' #zField
Ws0 @PushWFArc f2 '' #zField
Ws0 @UdInit f3 '' #zField
Ws0 @UdProcessEnd f4 '' #zField
Ws0 @PushWFArc f5 '' #zField
Ws0 @UdEvent f6 '' #zField
Ws0 @UdEvent f7 '' #zField
Ws0 @GridStep f8 '' #zField
Ws0 @GridStep f9 '' #zField
Ws0 @UdProcessEnd f10 '' #zField
Ws0 @UdProcessEnd f11 '' #zField
Ws0 @PushWFArc f12 '' #zField
Ws0 @PushWFArc f13 '' #zField
Ws0 @PushWFArc f14 '' #zField
Ws0 @PushWFArc f15 '' #zField
Ws0 @UdEvent f16 '' #zField
Ws0 @GridStep f17 '' #zField
Ws0 @UdProcessEnd f18 '' #zField
Ws0 @PushWFArc f19 '' #zField
Ws0 @PushWFArc f20 '' #zField
Ws0 @UdEvent f21 '' #zField
Ws0 @PushWFArc f22 '' #zField
Ws0 @UdMethod f23 '' #zField
Ws0 @PushWFArc f25 '' #zField
Ws0 @UdEvent f24 '' #zField
Ws0 @UdExitEnd f26 '' #zField
Ws0 @PushWFArc f27 '' #zField
>Proto Ws0 Ws0 WebServiceInformationProcess #zField
Ws0 f0 guid 178CFF78CDB9CB9F #txt
Ws0 f0 method start(entity.WebServiceDataEntity) #txt
Ws0 f0 inParameterDecl '<entity.WebServiceDataEntity demoWebserviceData> param;' #txt
Ws0 f0 inParameterMapAction 'out.demoWebserviceData.bean=param.demoWebserviceData;
' #txt
Ws0 f0 outParameterDecl '<entity.WebServiceDataEntity demoWebserviceData> result;' #txt
Ws0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(WebServiceDataEntity)</name>
    </language>
</elementInfo>
' #txt
Ws0 f0 61 77 22 22 24 -10 #rect
Ws0 f0 @|UdInitIcon #fIcon
Ws0 f1 61 173 22 22 14 0 #rect
Ws0 f1 @|UdProcessEndIcon #fIcon
Ws0 f2 72 99 72 173 #arcP
Ws0 f3 guid 178E84760163F87E #txt
Ws0 f3 method request() #txt
Ws0 f3 inParameterDecl '<> param;' #txt
Ws0 f3 inParameterMapAction 'out.demoWebserviceData.bean.name="";
out.demoWebserviceData.individualMaterialManufacturerInformationEntity.partNumber="";
out.demoWebserviceData.isRequest=true;
' #txt
Ws0 f3 outParameterDecl '<entity.WebServiceDataEntity data> result;' #txt
Ws0 f3 outParameterMapAction 'result.data=in.demoWebserviceData.bean;
' #txt
Ws0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>request</name>
    </language>
</elementInfo>
' #txt
Ws0 f3 61 221 22 22 24 -10 #rect
Ws0 f3 @|UdInitIcon #fIcon
Ws0 f4 61 309 22 22 14 0 #rect
Ws0 f4 @|UdProcessEndIcon #fIcon
Ws0 f5 72 243 72 309 #arcP
Ws0 f6 guid 178E8588848841E8 #txt
Ws0 f6 actionTable 'out=in;
' #txt
Ws0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isEdit</name>
    </language>
</elementInfo>
' #txt
Ws0 f6 309 77 22 22 16 -6 #rect
Ws0 f6 @|UdEventIcon #fIcon
Ws0 f7 guid 178E8588EC352032 #txt
Ws0 f7 actionTable 'out=in;
' #txt
Ws0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete</name>
    </language>
</elementInfo>
' #txt
Ws0 f7 749 77 22 22 14 0 #rect
Ws0 f7 @|UdEventIcon #fIcon
Ws0 f8 actionTable 'out=in;
out.demoWebserviceData.individualMaterialManufacturerInformationEntity=in.demoWebserviceData.individualMaterialManufacturerInformationEntity;
' #txt
Ws0 f8 actionCode 'import org.primefaces.context.RequestContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

FacesContext.getCurrentInstance().
                addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_INFO, 
                									"Edited","" ));
RequestContext.getCurrentInstance().update("form");
RequestContext.getCurrentInstance().update("msg");
RequestContext.getCurrentInstance().execute("PF(''individualMaterialManufacturerInformationDialog'').hide()");' #txt
Ws0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send Msg and update data</name>
    </language>
</elementInfo>
' #txt
Ws0 f8 302 236 36 24 -164 -6 #rect
Ws0 f8 @|StepIcon #fIcon
Ws0 f9 actionTable 'out=in;
' #txt
Ws0 f9 actionCode 'import org.primefaces.context.RequestContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

ivy.log.info(in.demoWebserviceData.bean.individualMaterialManufacturerInformation);

out.demoWebserviceData.bean.individualMaterialManufacturerInformation.remove(in.demoWebserviceData.individualMaterialManufacturerInformationEntity);
FacesContext.getCurrentInstance().
                addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_INFO, 
                									"Deleted record","" ));
RequestContext.getCurrentInstance().update("form");
RequestContext.getCurrentInstance().update("msg");
RequestContext.getCurrentInstance().execute("PF(''individualMaterialManufacturerInformationDialog'').hide()");


' #txt
Ws0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send Msg and update data</name>
    </language>
</elementInfo>
' #txt
Ws0 f9 742 196 36 24 -175 -7 #rect
Ws0 f9 @|StepIcon #fIcon
Ws0 f10 309 309 22 22 14 0 #rect
Ws0 f10 @|UdProcessEndIcon #fIcon
Ws0 f11 749 309 22 22 14 0 #rect
Ws0 f11 @|UdProcessEndIcon #fIcon
Ws0 f12 760 99 760 196 #arcP
Ws0 f13 760 220 760 309 #arcP
Ws0 f14 320 99 320 236 #arcP
Ws0 f15 320 260 320 309 #arcP
Ws0 f15 0 0.5694837909777933 0 0 #arcLabel
Ws0 f16 guid 178E86061B4C4CAC #txt
Ws0 f16 actionTable 'out=in;
' #txt
Ws0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isAdd</name>
    </language>
</elementInfo>
' #txt
Ws0 f16 1013 77 22 22 14 0 #rect
Ws0 f16 @|UdEventIcon #fIcon
Ws0 f17 actionTable 'out=in;
' #txt
Ws0 f17 actionCode 'import org.primefaces.context.RequestContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

ivy.log.info(in.demoWebserviceData.bean.individualMaterialManufacturerInformation);
out.demoWebserviceData.bean.individualMaterialManufacturerInformation.add(in.demoWebserviceData.individualMaterialManufacturerInformationEntity);
FacesContext.getCurrentInstance().
                addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_INFO, 
                									"New Individual Material Manufacturer Information was added","" ));
RequestContext.getCurrentInstance().update("form");
RequestContext.getCurrentInstance().update("msg");
RequestContext.getCurrentInstance().execute("PF(''individualMaterialManufacturerInformationDialog'').hide()");' #txt
Ws0 f17 security system #txt
Ws0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send Msg and update data</name>
    </language>
</elementInfo>
' #txt
Ws0 f17 1006 196 36 24 -178 -9 #rect
Ws0 f17 @|StepIcon #fIcon
Ws0 f18 1013 309 22 22 14 0 #rect
Ws0 f18 @|UdProcessEndIcon #fIcon
Ws0 f19 1024 99 1024 196 #arcP
Ws0 f20 1024 220 1024 309 #arcP
Ws0 f21 guid 178E8F9B7977EDE0 #txt
Ws0 f21 actionTable 'out=in;
out.demoWebserviceData.individualMaterialManufacturerInformationEntity=new entity.IndividualMaterialManufacturerInformationEntity();
out.headerDialog="Add new";
' #txt
Ws0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>preAdd</name>
    </language>
</elementInfo>
' #txt
Ws0 f21 1085 77 22 22 14 -2 #rect
Ws0 f21 @|UdEventIcon #fIcon
Ws0 f22 1096 99 1035 320 #arcP
Ws0 f22 1 1096 312 #addKink
Ws0 f22 2 1088 320 #addKink
Ws0 f22 0 0.6814480044518187 0 0 #arcLabel
Ws0 f23 guid 178E8FE799D5BE9D #txt
Ws0 f23 method preEdit(entity.IndividualMaterialManufacturerInformationEntity) #txt
Ws0 f23 inParameterDecl '<entity.IndividualMaterialManufacturerInformationEntity object> param;' #txt
Ws0 f23 inParameterMapAction 'out.demoWebserviceData.individualMaterialManufacturerInformationEntity=param.object;
out.headerDialog="Edit";
' #txt
Ws0 f23 outParameterDecl '<> result;' #txt
Ws0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>preEdit(IndividualMaterialManufacturerInformationEntity)</name>
    </language>
</elementInfo>
' #txt
Ws0 f23 381 77 22 22 20 -7 #rect
Ws0 f23 @|UdMethodIcon #fIcon
Ws0 f25 392 99 331 320 #arcP
Ws0 f25 1 392 312 #addKink
Ws0 f25 2 384 320 #addKink
Ws0 f25 0 0.1719153108865537 0 0 #arcLabel
Ws0 f24 guid 178E92A46615D55B #txt
Ws0 f24 actionTable 'out=in;
' #txt
Ws0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ws0 f24 1189 77 22 22 16 -10 #rect
Ws0 f24 @|UdEventIcon #fIcon
Ws0 f26 1189 309 22 22 14 0 #rect
Ws0 f26 @|UdExitEndIcon #fIcon
Ws0 f27 1200 99 1200 309 #arcP
>Proto Ws0 .type trainning.WebServiceInformation.WebServiceInformationData #txt
>Proto Ws0 .processKind HTML_DIALOG #txt
>Proto Ws0 -8 -8 16 16 16 26 #rect
>Proto Ws0 '' #fIcon
Ws0 f0 mainOut f2 tail #connect
Ws0 f2 head f1 mainIn #connect
Ws0 f3 mainOut f5 tail #connect
Ws0 f5 head f4 mainIn #connect
Ws0 f7 mainOut f12 tail #connect
Ws0 f12 head f9 mainIn #connect
Ws0 f9 mainOut f13 tail #connect
Ws0 f13 head f11 mainIn #connect
Ws0 f6 mainOut f14 tail #connect
Ws0 f14 head f8 mainIn #connect
Ws0 f8 mainOut f15 tail #connect
Ws0 f15 head f10 mainIn #connect
Ws0 f16 mainOut f19 tail #connect
Ws0 f19 head f17 mainIn #connect
Ws0 f17 mainOut f20 tail #connect
Ws0 f20 head f18 mainIn #connect
Ws0 f21 mainOut f22 tail #connect
Ws0 f22 head f18 mainIn #connect
Ws0 f23 mainOut f25 tail #connect
Ws0 f25 head f10 mainIn #connect
Ws0 f24 mainOut f27 tail #connect
Ws0 f27 head f26 mainIn #connect
