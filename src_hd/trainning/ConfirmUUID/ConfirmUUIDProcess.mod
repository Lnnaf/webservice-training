[Ivy]
178E9B13E17C3E3F 7.5.0 #module
>Proto >Proto Collection #zClass
Cs0 ConfirmUUIDProcess Big #zClass
Cs0 RD #cInfo
Cs0 #process
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @UdInit f0 '' #zField
Cs0 @UdProcessEnd f1 '' #zField
Cs0 @PushWFArc f2 '' #zField
Cs0 @UdEvent f3 '' #zField
Cs0 @UdExitEnd f4 '' #zField
Cs0 @WSElement f6 '' #zField
Cs0 @PushWFArc f7 '' #zField
Cs0 @PushWFArc f5 '' #zField
>Proto Cs0 Cs0 ConfirmUUIDProcess #zField
Cs0 f0 guid 178E9B13E1BA7A8D #txt
Cs0 f0 method start() #txt
Cs0 f0 inParameterDecl '<> param;' #txt
Cs0 f0 outParameterDecl '<> result;' #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Cs0 f0 85 53 22 22 14 0 #rect
Cs0 f0 @|UdInitIcon #fIcon
Cs0 f1 85 117 22 22 14 0 #rect
Cs0 f1 @|UdProcessEndIcon #fIcon
Cs0 f2 96 75 96 117 #arcP
Cs0 f3 guid 178E9B13E271446A #txt
Cs0 f3 actionTable 'out=in;
' #txt
Cs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Cs0 f3 341 53 22 22 14 0 #rect
Cs0 f3 @|UdEventIcon #fIcon
Cs0 f4 341 181 22 22 14 0 #rect
Cs0 f4 @|UdExitEndIcon #fIcon
Cs0 f6 actionTable 'out=in;
' #txt
Cs0 f6 clientId 178CE5881963D7AF #txt
Cs0 f6 port ConfirmationWebServicePort #txt
Cs0 f6 operation call #txt
Cs0 f6 inputParams 'parameters.data.uuid=in.uuid;
' #txt
Cs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Request send uuid</name>
    </language>
</elementInfo>
' #txt
Cs0 f6 334 116 36 24 20 -2 #rect
Cs0 f6 @|WebServiceIcon #fIcon
Cs0 f7 352 75 352 116 #arcP
Cs0 f5 352 140 352 181 #arcP
>Proto Cs0 .type trainning.ConfirmUUID.ConfirmUUIDData #txt
>Proto Cs0 .processKind HTML_DIALOG #txt
>Proto Cs0 -8 -8 16 16 16 26 #rect
>Proto Cs0 '' #fIcon
Cs0 f0 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
Cs0 f3 mainOut f7 tail #connect
Cs0 f7 head f6 mainIn #connect
Cs0 f6 mainOut f5 tail #connect
Cs0 f5 head f4 mainIn #connect
