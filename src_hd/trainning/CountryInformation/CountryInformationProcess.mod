[Ivy]
178D8AAE5C55BC6F 7.5.0 #module
>Proto >Proto Collection #zClass
Cs0 CountryInformationProcess Big #zClass
Cs0 RD #cInfo
Cs0 #process
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @UdInit f0 '' #zField
Cs0 @UdProcessEnd f1 '' #zField
Cs0 @PushWFArc f2 '' #zField
Cs0 @UdEvent f3 '' #zField
Cs0 @WSElement f6 '' #zField
Cs0 @PushWFArc f7 '' #zField
Cs0 @UdProcessEnd f4 '' #zField
Cs0 @GridStep f8 '' #zField
Cs0 @PushWFArc f5 '' #zField
Cs0 @RestClientCall f10 '' #zField
Cs0 @PushWFArc f9 '' #zField
Cs0 @Alternative f12 '' #zField
Cs0 @PushWFArc f13 '' #zField
Cs0 @PushWFArc f11 '' #zField
Cs0 @PushWFArc f14 '' #zField
>Proto Cs0 Cs0 CountryInformationProcess #zField
Cs0 f0 guid 178D8AAE5CE82F67 #txt
Cs0 f0 method start(trainning.CountryInformationData) #txt
Cs0 f0 inParameterDecl '<trainning.CountryInformationData countryInformationData> param;' #txt
Cs0 f0 inParameterMapAction 'out.countryInformationData=param.countryInformationData;
out.countryInformationData.isHaveData=false;
' #txt
Cs0 f0 outParameterDecl '<trainning.CountryInformationData countryInformationData> result;' #txt
Cs0 f0 outParameterMapAction 'result.countryInformationData=in.countryInformationData;
' #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(CountryInformationData)</name>
    </language>
</elementInfo>
' #txt
Cs0 f0 85 53 22 22 14 0 #rect
Cs0 f0 @|UdInitIcon #fIcon
Cs0 f1 85 125 22 22 14 0 #rect
Cs0 f1 @|UdProcessEndIcon #fIcon
Cs0 f2 96 75 96 125 #arcP
Cs0 f3 guid 178D8AAE5D7E5056 #txt
Cs0 f3 actionTable 'out=in;
' #txt
Cs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>find</name>
    </language>
</elementInfo>
' #txt
Cs0 f3 325 53 22 22 13 -11 #rect
Cs0 f3 @|UdEventIcon #fIcon
Cs0 f6 actionTable 'out=in;
out.countryInformationData.capitalCity=wsResponse.sCapitalCity;
out.countryInformationData.continentCode=wsResponse.sContinentCode;
out.countryInformationData.countryFlag=wsResponse.sCountryFlag;
out.countryInformationData.countryISOCode=wsResponse.sISOCode;
out.countryInformationData.CurencyCode=wsResponse.sCurrencyISOCode;
out.countryInformationData.isHaveData=true;
out.countryInformationData.language=wsResponse.languages;
out.countryInformationData.name=wsResponse.sName;
out.countryInformationData.phoneCode=wsResponse.sPhoneCode;
' #txt
Cs0 f6 clientId 178D89A58E7817DC #txt
Cs0 f6 port CountryInfoServiceSoap #txt
Cs0 f6 operation FullCountryInfo #txt
Cs0 f6 inputParams 'parameters.sCountryISOCode=in.countryInformationData.countryISOCode;
' #txt
Cs0 f6 318 132 36 24 20 -2 #rect
Cs0 f6 @|WebServiceIcon #fIcon
Cs0 f7 336 75 336 132 #arcP
Cs0 f4 325 485 22 22 14 0 #rect
Cs0 f4 @|UdProcessEndIcon #fIcon
Cs0 f8 actionTable 'out=in;
' #txt
Cs0 f8 actionCode 'import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
if(in.countryInformationData.name=="Country not found in the database"){
in.countryInformationData.isHaveData=false;
FacesContext.getCurrentInstance().
                addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                									"Country not found, check ISO Code and try again !","" ));
}else{
in.countryInformationData.isHaveData=true;

}
ivy.log.info(in.countryInformationData.openWeather.timezone);' #txt
Cs0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Check Data</name>
    </language>
</elementInfo>
' #txt
Cs0 f8 318 364 36 24 21 -7 #rect
Cs0 f8 @|StepIcon #fIcon
Cs0 f5 336 388 336 485 #arcP
Cs0 f10 clientId 1cef7f05-0c75-44c3-b9c9-3f8218a2a688 #txt
Cs0 f10 queryParams 'q=in.countryInformationData.capitalCity;
appid="76b315504dd2d96e6a11dd62b6337ded";
units="metric";
' #txt
Cs0 f10 headers 'Accept=application/json;
' #txt
Cs0 f10 resultType entity.OpenWeatherEntity #txt
Cs0 f10 responseCode 'ivy.log.info("================"+response);
in.countryInformationData.openWeather = result;' #txt
Cs0 f10 clientErrorCode ivy:error:rest:client #txt
Cs0 f10 statusErrorCode ivy:error:rest:client #txt
Cs0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get curent weather status of city</name>
    </language>
</elementInfo>
' #txt
Cs0 f10 318 268 36 24 20 -2 #rect
Cs0 f10 @|RestClientCallIcon #fIcon
Cs0 f9 336 292 336 364 #arcP
Cs0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Handle param may be null</name>
    </language>
</elementInfo>
' #txt
Cs0 f12 322 178 28 28 15 -7 #rect
Cs0 f12 @|AlternativeIcon #fIcon
Cs0 f13 336 156 336 178 #arcP
Cs0 f11 expr in #txt
Cs0 f11 outCond in.countryInformationData.capitalCity!="" #txt
Cs0 f11 336 206 336 268 #arcP
Cs0 f14 expr in #txt
Cs0 f14 322 192 318 376 #arcP
Cs0 f14 1 296 192 #addKink
Cs0 f14 2 288 200 #addKink
Cs0 f14 3 288 368 #addKink
Cs0 f14 4 296 376 #addKink
Cs0 f14 3 0.1934843827112777 0 0 #arcLabel
>Proto Cs0 .type trainning.CountryInformation.CountryInformationData #txt
>Proto Cs0 .processKind HTML_DIALOG #txt
>Proto Cs0 -8 -8 16 16 16 26 #rect
>Proto Cs0 '' #fIcon
Cs0 f0 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
Cs0 f3 mainOut f7 tail #connect
Cs0 f7 head f6 mainIn #connect
Cs0 f8 mainOut f5 tail #connect
Cs0 f5 head f4 mainIn #connect
Cs0 f10 mainOut f9 tail #connect
Cs0 f9 head f8 mainIn #connect
Cs0 f6 mainOut f13 tail #connect
Cs0 f13 head f12 in #connect
Cs0 f12 out f11 tail #connect
Cs0 f11 head f10 mainIn #connect
Cs0 f12 out f14 tail #connect
Cs0 f14 head f8 mainIn #connect
