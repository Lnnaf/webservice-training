[Ivy]
178E83A40B019873 7.5.0 #module
>Proto >Proto Collection #zClass
Ws0 WsDemos Big #zClass
Ws0 B #cInfo
Ws0 #process
Ws0 @AnnotationInP-0n ai ai #zField
Ws0 @TextInP .type .type #zField
Ws0 @TextInP .processKind .processKind #zField
Ws0 @TextInP .xml .xml #zField
Ws0 @TextInP .responsibility .responsibility #zField
Ws0 @StartRequest f0 '' #zField
Ws0 @EndTask f1 '' #zField
Ws0 @UserDialog f5 '' #zField
Ws0 @PushWFArc f6 '' #zField
Ws0 @WSElement f2 '' #zField
Ws0 @PushWFArc f3 '' #zField
Ws0 @PushWFArc f4 '' #zField
>Proto Ws0 Ws0 WsDemos #zField
Ws0 f0 outLink start.ivp #txt
Ws0 f0 inParamDecl '<> param;' #txt
Ws0 f0 requestEnabled true #txt
Ws0 f0 triggerEnabled false #txt
Ws0 f0 callSignature start() #txt
Ws0 f0 caseData businessCase.attach=true #txt
Ws0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Ws0 f0 @C|.responsibility Everybody #txt
Ws0 f0 43 51 26 26 -20 -30 #rect
Ws0 f0 @|StartRequestIcon #fIcon
Ws0 f1 411 51 26 26 14 0 #rect
Ws0 f1 @|EndIcon #fIcon
Ws0 f5 dialogId trainning.WebServiceInformation #txt
Ws0 f5 startMethod request() #txt
Ws0 f5 requestActionDecl '<> param;' #txt
Ws0 f5 responseMappingAction 'out=in;
out.abc.name=result.data.name;
out.abc.serialId=result.data.serialId;
' #txt
Ws0 f5 responseActionCode 'import entity.IndividualMaterialManufacturerInformationEntity;
for(IndividualMaterialManufacturerInformationEntity a: result.data.individualMaterialManufacturerInformation){
out.obj.partNumber = a.partNumber;
out.obj.partyInternalID = a.partyInternalID;
out.obj.partyInternalName = a.partyInternalName;
out.abc.individualMaterialManufacturerInformation.add(out.obj);
}' #txt
Ws0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>WebServiceInformation</name>
    </language>
</elementInfo>
' #txt
Ws0 f5 140 52 40 24 -48 -34 #rect
Ws0 f5 @|UserDialogIcon #fIcon
Ws0 f6 69 64 140 64 #arcP
Ws0 f2 actionTable 'out=in;
' #txt
Ws0 f2 clientId 178CA086E8A3D133 #txt
Ws0 f2 port DemoWebservicePort #txt
Ws0 f2 operation call #txt
Ws0 f2 inputParams 'parameters.data=in.abc;
' #txt
Ws0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Ws Request</name>
    </language>
</elementInfo>
' #txt
Ws0 f2 304 52 32 24 -22 -36 #rect
Ws0 f2 @|WebServiceIcon #fIcon
Ws0 f3 180 64 304 64 #arcP
Ws0 f4 336 64 411 64 #arcP
>Proto Ws0 .type trainning.Data #txt
>Proto Ws0 .processKind NORMAL #txt
>Proto Ws0 0 0 32 24 18 0 #rect
>Proto Ws0 @|BIcon #fIcon
Ws0 f0 mainOut f6 tail #connect
Ws0 f6 head f5 mainIn #connect
Ws0 f5 mainOut f3 tail #connect
Ws0 f3 head f2 mainIn #connect
Ws0 f2 mainOut f4 tail #connect
Ws0 f4 head f1 mainIn #connect
