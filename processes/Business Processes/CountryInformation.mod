[Ivy]
178D89E77D0BBA22 7.5.0 #module
>Proto >Proto Collection #zClass
Cn0 CountryInformation Big #zClass
Cn0 B #cInfo
Cn0 #process
Cn0 @AnnotationInP-0n ai ai #zField
Cn0 @TextInP .type .type #zField
Cn0 @TextInP .processKind .processKind #zField
Cn0 @TextInP .xml .xml #zField
Cn0 @TextInP .responsibility .responsibility #zField
Cn0 @StartRequest f0 '' #zField
Cn0 @EndTask f1 '' #zField
Cn0 @UserDialog f5 '' #zField
Cn0 @PushWFArc f2 '' #zField
Cn0 @PushWFArc f3 '' #zField
>Proto Cn0 Cn0 CountryInformation #zField
Cn0 f0 outLink start.ivp #txt
Cn0 f0 inParamDecl '<> param;' #txt
Cn0 f0 requestEnabled true #txt
Cn0 f0 triggerEnabled false #txt
Cn0 f0 callSignature start() #txt
Cn0 f0 caseData businessCase.attach=true #txt
Cn0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Cn0 f0 @C|.responsibility Everybody #txt
Cn0 f0 83 51 26 26 -7 -33 #rect
Cn0 f0 @|StartRequestIcon #fIcon
Cn0 f1 355 51 26 26 14 0 #rect
Cn0 f1 @|EndIcon #fIcon
Cn0 f5 dialogId trainning.CountryInformation #txt
Cn0 f5 startMethod start(trainning.CountryInformationData) #txt
Cn0 f5 requestActionDecl '<trainning.CountryInformationData countryInformationData> param;' #txt
Cn0 f5 requestMappingAction 'param.countryInformationData=in;
' #txt
Cn0 f5 responseMappingAction 'out=in;
out=result.countryInformationData;
' #txt
Cn0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>CountryInformation</name>
    </language>
</elementInfo>
' #txt
Cn0 f5 198 52 36 24 -30 -35 #rect
Cn0 f5 @|UserDialogIcon #fIcon
Cn0 f2 234 64 355 64 #arcP
Cn0 f2 0 0.5000000000000001 0 0 #arcLabel
Cn0 f3 109 64 198 64 #arcP
>Proto Cn0 .type trainning.CountryInformationData #txt
>Proto Cn0 .processKind NORMAL #txt
>Proto Cn0 0 0 32 24 18 0 #rect
>Proto Cn0 @|BIcon #fIcon
Cn0 f5 mainOut f2 tail #connect
Cn0 f2 head f1 mainIn #connect
Cn0 f0 mainOut f3 tail #connect
Cn0 f3 head f5 mainIn #connect
