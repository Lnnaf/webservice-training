[Ivy]
178D379651417B38 7.5.0 #module
>Proto >Proto Collection #zClass
De0 DemoWebservice Big #zClass
De0 WS #cInfo
De0 #process
De0 @TextInP .webServiceName .webServiceName #zField
De0 @TextInP .implementationClassName .implementationClassName #zField
De0 @TextInP .authenticationType .authenticationType #zField
De0 @AnnotationInP-0n ai ai #zField
De0 @TextInP .type .type #zField
De0 @TextInP .processKind .processKind #zField
De0 @TextInP .xml .xml #zField
De0 @TextInP .responsibility .responsibility #zField
De0 @StartWS f0 '' #zField
De0 @EndWS f1 '' #zField
De0 @GridStep f7 '' #zField
De0 @TaskSwitchSimple f4 '' #zField
De0 @IntermediateEvent f3 '' #zField
De0 @UserDialog f6 '' #zField
De0 @PushWFArc f8 '' #zField
De0 @PushWFArc f14 '' #zField
De0 @PushWFArc f9 '' #zField
De0 @PushWFArc f2 '' #zField
De0 @ProcessException f11 '' #zField
De0 @EndTask f12 '' #zField
De0 @GridStep f10 '' #zField
De0 @PushWFArc f13 '' #zField
De0 @PushWFArc f15 '' #zField
De0 @TkArc f5 '' #zField
De0 @PushWFArc f17 '' #zField
De0 @GridStep f16 '' #zField
>Proto De0 De0 DemoWebservice #zField
De0 f0 inParamDecl '<entity.WebServiceDataEntity data> param;' #txt
De0 f0 inParamTable 'out.bean=param.data;
' #txt
De0 f0 outParamDecl '<> result;' #txt
De0 f0 callSignature call(entity.WebServiceDataEntity) #txt
De0 f0 useUserDefinedException false #txt
De0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(WebServiceDataEntity)</name>
    </language>
</elementInfo>
' #txt
De0 f0 @C|.responsibility Everybody #txt
De0 f0 51 51 26 26 -26 -30 #rect
De0 f0 @|StartWSIcon #fIcon
De0 f1 899 51 26 26 14 0 #rect
De0 f1 @|EndWSIcon #fIcon
De0 f7 actionTable 'out=in;
' #txt
De0 f7 actionCode 'import ch.ivyteam.ivy.process.model.value.SignalCode;
import javax.faces.context.Flash;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
import java.util.UUID;
out.bean.uuid = UUID.randomUUID().toString();
ivy.log.info(out.bean.uuid);
' #txt
De0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>random uuid</name>
    </language>
</elementInfo>
' #txt
De0 f7 214 52 36 24 -25 -33 #rect
De0 f7 @|StepIcon #fIcon
De0 f4 actionTable 'out=in1;
' #txt
De0 f4 taskData TaskA.NAM=<%\=ivy.cms.co("/Label/outputWebservice")%> #txt
De0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>System Task&#13;
</name>
    </language>
</elementInfo>
' #txt
De0 f4 635 51 26 26 -15 -33 #rect
De0 f4 @|TaskSwitchSimpleIcon #fIcon
De0 f3 actionTable 'out=in;
' #txt
De0 f3 eventIdConfig "in.bean.uuid" #txt
De0 f3 timeoutConfig 'ACTION_AFTER_TIMEOUT=DESTROY_TASK
EXCEPTION_PROCESS_START=178D379651417B38-f11-buffer
TIMEOUT_SCRIPT=new Duration(ivy.var.escalation_webservice)' #txt
De0 f3 eventBeanClass "" #txt
De0 f3 eventBeanConfig "" #txt
De0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Wait confirmation UUID</name>
    </language>
</elementInfo>
' #txt
De0 f3 340 52 24 24 -36 -34 #rect
De0 f3 @|IntermediateEventIcon #fIcon
De0 f6 dialogId trainning.WebServiceInformation #txt
De0 f6 startMethod start(entity.WebServiceDataEntity) #txt
De0 f6 requestActionDecl '<entity.WebServiceDataEntity demoWebserviceData> param;' #txt
De0 f6 requestMappingAction 'param.demoWebserviceData=in.bean;
' #txt
De0 f6 responseMappingAction 'out=in;
' #txt
De0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>WebServiceInformation</name>
    </language>
</elementInfo>
' #txt
De0 f6 766 52 36 24 -31 -33 #rect
De0 f6 @|UserDialogIcon #fIcon
De0 f8 250 64 340 64 #arcP
De0 f8 0 0.5000000000000002 0 0 #arcLabel
De0 f14 661 64 766 64 #arcP
De0 f9 77 64 214 64 #arcP
De0 f2 802 64 899 64 #arcP
De0 f11 actionTable 'out=in;
' #txt
De0 f11 errorCode ivy:error:program:timeout #txt
De0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>timeout</name>
    </language>
</elementInfo>
' #txt
De0 f11 340 156 24 24 -1 12 #rect
De0 f11 @|ExceptionIcon #fIcon
De0 f12 571 155 26 26 14 0 #rect
De0 f12 @|EndIcon #fIcon
De0 f10 actionTable 'out=in;
' #txt
De0 f10 actionCode 'import service.LogInfoService;
LogInfoService log = new service.LogInfoService();
ivy.log.info("===== logged");
log.writeLogTimeOut("Event id ["+out.bean.uuid+"] escalation timeout!");

' #txt
De0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Log Timeout</name>
    </language>
</elementInfo>
' #txt
De0 f10 446 156 36 24 -14 14 #rect
De0 f10 @|StepIcon #fIcon
De0 f13 364 168 446 168 #arcP
De0 f15 482 168 571 168 #arcP
De0 f5 530 64 635 64 #arcP
De0 f17 364 64 494 64 #arcP
De0 f16 actionTable 'out=in;
' #txt
De0 f16 actionCode 'import repository.WebServiceDataRepository;
WebServiceDataRepository repo = new repository.WebServiceDataRepository();
repo.save(in.bean);
ivy.log.info("======== save obj uuid: "+in.bean.uuid);' #txt
De0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>save obj to database</name>
    </language>
</elementInfo>
' #txt
De0 f16 494 52 36 24 -19 -34 #rect
De0 f16 @|StepIcon #fIcon
>Proto De0 .webServiceName trainning.DemoWebservice #txt
>Proto De0 .type trainning.Data #txt
>Proto De0 .processKind WEB_SERVICE #txt
>Proto De0 -8 -8 16 16 16 26 #rect
>Proto De0 '' #fIcon
De0 f7 mainOut f8 tail #connect
De0 f8 head f3 mainIn #connect
De0 f4 out f14 tail #connect
De0 f14 head f6 mainIn #connect
De0 f0 mainOut f9 tail #connect
De0 f9 head f7 mainIn #connect
De0 f6 mainOut f2 tail #connect
De0 f2 head f1 mainIn #connect
De0 f11 mainOut f13 tail #connect
De0 f13 head f10 mainIn #connect
De0 f10 mainOut f15 tail #connect
De0 f15 head f12 mainIn #connect
De0 f3 mainOut f17 tail #connect
De0 f17 head f16 mainIn #connect
De0 f16 mainOut f5 tail #connect
De0 f5 head f4 in #connect
