[Ivy]
178D37E748C8DBE1 7.5.0 #module
>Proto >Proto Collection #zClass
Ce0 ConfirmationWebService Big #zClass
Ce0 WS #cInfo
Ce0 #process
Ce0 @TextInP .webServiceName .webServiceName #zField
Ce0 @TextInP .implementationClassName .implementationClassName #zField
Ce0 @TextInP .authenticationType .authenticationType #zField
Ce0 @AnnotationInP-0n ai ai #zField
Ce0 @TextInP .type .type #zField
Ce0 @TextInP .processKind .processKind #zField
Ce0 @TextInP .xml .xml #zField
Ce0 @TextInP .responsibility .responsibility #zField
Ce0 @EndWS f1 '' #zField
Ce0 @GridStep f3 '' #zField
Ce0 @PushWFArc f2 '' #zField
Ce0 @PushWFArc f4 '' #zField
Ce0 @StartWS f0 '' #zField
>Proto Ce0 Ce0 ConfirmationWebService #zField
Ce0 f1 339 51 26 26 14 0 #rect
Ce0 f1 @|EndWSIcon #fIcon
Ce0 f3 actionTable 'out=in;
' #txt
Ce0 f3 actionCode 'import utils.IvyEventUtils;
IvyEventUtils.fireIntermediateEventByIdWildcard(in.bean.uuid, null);' #txt
Ce0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Firer wai confirmation UUID</name>
    </language>
</elementInfo>
' #txt
Ce0 f3 206 52 36 24 -11 -34 #rect
Ce0 f3 @|StepIcon #fIcon
Ce0 f2 242 64 339 64 #arcP
Ce0 f4 109 64 206 64 #arcP
Ce0 f0 inParamDecl '<entity.WebServiceDataEntity data> param;' #txt
Ce0 f0 inParamTable 'out.bean=param.data;
' #txt
Ce0 f0 outParamDecl '<> result;' #txt
Ce0 f0 callSignature call(entity.WebServiceDataEntity) #txt
Ce0 f0 useUserDefinedException false #txt
Ce0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(WebServiceDataEntity)</name>
    </language>
</elementInfo>
' #txt
Ce0 f0 @C|.responsibility Everybody #txt
Ce0 f0 83 51 26 26 -45 -33 #rect
Ce0 f0 @|StartWSIcon #fIcon
>Proto Ce0 .webServiceName trainning.ConfirmationWebService #txt
>Proto Ce0 .type trainning.Data #txt
>Proto Ce0 .processKind WEB_SERVICE #txt
>Proto Ce0 -8 -8 16 16 16 26 #rect
>Proto Ce0 '' #fIcon
Ce0 f0 mainOut f4 tail #connect
Ce0 f4 head f3 mainIn #connect
Ce0 f3 mainOut f2 tail #connect
Ce0 f2 head f1 mainIn #connect
