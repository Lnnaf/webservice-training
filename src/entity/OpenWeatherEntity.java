package entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpenWeatherEntity {
    @SerializedName("coord")
    @Expose
    private Object coord;
    @SerializedName("weather")
    @Expose
    private Object weather;
    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("main")
    @Expose
    private Object main;
    @SerializedName("visibility")
    @Expose
    private Long visibility;
    @SerializedName("wind")
    @Expose
    private Object wind;
    @SerializedName("clouds")
    @Expose
    private Object clouds;
    @SerializedName("dt")
    @Expose
    private String dt;
    @SerializedName("sys")
    @Expose
    private Object sys;
    @SerializedName("timezone")
    @Expose
    private Long timezone;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cod")
    @Expose
    private Integer cod;
	public Object getCoord() {
		return coord;
	}
	public void setCoord(Object coord) {
		this.coord = coord;
	}
	public Object getWeather() {
		return weather;
	}
	public void setWeather(Object weather) {
		this.weather = weather;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public Object getMain() {
		return main;
	}
	public void setMain(Object main) {
		this.main = main;
	}
	public Long getVisibility() {
		return visibility;
	}
	public void setVisibility(Long visibility) {
		this.visibility = visibility;
	}
	public Object getWind() {
		return wind;
	}
	public void setWind(Object wind) {
		this.wind = wind;
	}
	public Object getClouds() {
		return clouds;
	}
	public void setClouds(Object clouds) {
		this.clouds = clouds;
	}
	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	public Object getSys() {
		return sys;
	}
	public void setSys(Object sys) {
		this.sys = sys;
	}
	
	public Long getTimezone() {
		return timezone;
	}
	public void setTimezone(Long timezone) {
		this.timezone = timezone;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCod() {
		return cod;
	}
	public void setCod(Integer cod) {
		this.cod = cod;
	}
    

}
