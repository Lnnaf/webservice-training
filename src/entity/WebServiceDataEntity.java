package entity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "uuid", "name", 
		"serialId", "individualMaterialManufacturerInformation" })

public class WebServiceDataEntity {

	private String uuid;

	private String name;
	private String serialId;
	private List<IndividualMaterialManufacturerInformationEntity> individualMaterialManufacturerInformation;
	public WebServiceDataEntity(String uuid, String name, String serialId,
			List<IndividualMaterialManufacturerInformationEntity> individualMaterialManufacturerInformation) {
		super();
		this.uuid = uuid;
		this.name = name;
		this.serialId = serialId;
		this.individualMaterialManufacturerInformation = individualMaterialManufacturerInformation;
	}
	public WebServiceDataEntity() {}
	@XmlElement(name = "Uuid")
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	@XmlElement(name = "Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name = "SerialId")
	public String getSerialId() {
		return serialId;
	}
	public void setSerialId(String serialId) {
		this.serialId = serialId;
	}
	@XmlElement(name = "IndividualMaterialManufacturerInformation", required = true)
	public List<IndividualMaterialManufacturerInformationEntity> getIndividualMaterialManufacturerInformation() {
		return individualMaterialManufacturerInformation;
	}
	public void setIndividualMaterialManufacturerInformation(
			List<IndividualMaterialManufacturerInformationEntity> individualMaterialManufacturerInformation) {
		this.individualMaterialManufacturerInformation = individualMaterialManufacturerInformation;
	}
	
	
}
