package entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "partyInternalID", "partyInternalName", "partNumber"})
public class IndividualMaterialManufacturerInformationEntity {
	private String partyInternalID;
	private String partyInternalName;
	private String partNumber;
	public IndividualMaterialManufacturerInformationEntity(String partyInternalID, String partyInternalName,
			String partNumber) {
		this.partyInternalID = partyInternalID;
		this.partyInternalName = partyInternalName;
		this.partNumber = partNumber;
	}
	public IndividualMaterialManufacturerInformationEntity() {}
	@XmlElement(name = "PartyInternalID")
	public String getPartyInternalID() {
		return partyInternalID;
	}
	public void setPartyInternalID(String partyInternalID) {
		this.partyInternalID = partyInternalID;
	}
	@XmlElement(name = "PartyInternalName")
	public String getPartyInternalName() {
		return partyInternalName;
	}
	public void setPartyInternalName(String partyInternalName) {
		this.partyInternalName = partyInternalName;
	}
	@XmlElement(name = "PartNumber")
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

}
