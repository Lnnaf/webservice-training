package utils;

import java.util.concurrent.Callable;

import org.apache.commons.collections.CollectionUtils;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.IQueryResult;
import ch.ivyteam.ivy.security.SecurityManagerFactory;
import ch.ivyteam.ivy.workflow.IIntermediateEvent;
import ch.ivyteam.ivy.workflow.IPropertyFilter;
import ch.ivyteam.ivy.workflow.IntermediateEventProperty;
import ch.ivyteam.ivy.workflow.IntermediateEventState;
import ch.ivyteam.logicalexpression.RelationalOperator;

public class IvyEventUtils {
	
	public static void fireIntermediateEventByIdWildcard(String id, final Object param) throws Exception{

		SecurityManagerFactory.getSecurityManager().executeAsSystem(new Callable<Object>() {
			public Object call() throws Exception {
				Ivy.log().info("Searching for Intermediate Events with Id: "+ id);
				
				IPropertyFilter<IntermediateEventProperty> filter = Ivy.wf().createIntermediateEventPropertyFilter(IntermediateEventProperty.EVENT_ID, RelationalOperator.EQUAL, id);
				IQueryResult<IIntermediateEvent> queryResult = Ivy.wf().findIntermediateEvents(filter, null, 0, 1, true);
				
				if(CollectionUtils.isNotEmpty(queryResult.getResultList())){
					IIntermediateEvent intermediateEvent = queryResult.getResultList().get(0);
					if(intermediateEvent.getState().equals(IntermediateEventState.WAITING)){
						Ivy.log().info("Firing Intermed.Event  with id: "+ intermediateEvent.getEventIdentifier() );
						Ivy.wf().fireIntermediateEvent(intermediateEvent.getIntermediateEventElement(), intermediateEvent.getEventIdentifier(), param, null);
					}
				}else{
					Ivy.log().info("No Intermediate Events with Id: "+ id);
				}
				return null;
			}
		});
	}
}