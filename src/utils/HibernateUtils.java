package utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import ch.ivyteam.ivy.environment.Ivy;

public class HibernateUtils {
	
	public static SessionFactory getSessionFactory() {

		SessionFactory sessionFactory = null;
		try {
			Configuration configuration = new Configuration();
			configuration.configure();
			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
			builder.applySettings(configuration.getProperties());
			StandardServiceRegistry serviceRegistry = builder.build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			ex.printStackTrace();
			Ivy.log().info("=====================" + ex.getMessage());

		}
		return sessionFactory;
	}
}
