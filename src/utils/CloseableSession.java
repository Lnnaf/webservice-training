package utils;

import java.io.Closeable;

import org.hibernate.Session;

public class CloseableSession implements Closeable {

    private final Session session;

    public CloseableSession(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public void close() {
        session.close();
    }
    
}