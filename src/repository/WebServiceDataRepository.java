package repository;

import ch.ivyteam.ivy.environment.Ivy;
import entity.WebServiceDataEntity;

public class WebServiceDataRepository extends GenericRepository<WebServiceDataEntity> {

	@Override
	public Class<WebServiceDataEntity> getType() {
	
		return WebServiceDataEntity.class;
	}
	
	public WebServiceDataEntity findByUuid(String uuid) {
		WebServiceDataEntity result = Ivy.repo().search(WebServiceDataEntity.class)
			       .textField("uuid").containsAnyWords(uuid)
			       .execute()
			       .getFirst();
		return result;
	}
	
	public void deleteByUuid(String uuid) {
		WebServiceDataEntity result = Ivy.repo().search(WebServiceDataEntity.class)
			       .textField("uuid").containsAnyWords(uuid)
			       .execute()
			       .getFirst();
			Ivy.repo().delete(result);
	}

}
