package repository;

import java.util.List;

import ch.ivyteam.ivy.environment.Ivy;

/**
 * @author pdminh
 *
 * @param <T>
 */
public abstract class GenericRepository<T> implements IGenericRepository<T> {
	private static int DEFAULT_SEARCH_LIMIT = 5000;

	@Override
	public T findById(String id) {
		T obj = (T) Ivy.repo().find(id, getType());
		if (obj != null) {
			return obj;
		}
		return null;
	}

	@Override
	public T save(T obj) {
		Ivy.repo().save(obj);
		return obj;
	}

	@Override
	public void delete(T obj) {
		Ivy.repo().delete(obj);
	}

	@Override
	public void deleteById(String id) {
		Ivy.repo().deleteById(id);

	}

	@Override
	public List<T> findAll() {
		return Ivy.repo().search(getType()).limit(DEFAULT_SEARCH_LIMIT).execute().getAll();
	}

	@Override
	public List<T> saveAll(List<T> objs) {
		for (T t : objs) {
			save(t);
		}

		return findAll();
	}

	@Override
	public void deleteAll(List<T> objs) {
		for (T t : objs) {
			delete(t);
		}
	}

}
