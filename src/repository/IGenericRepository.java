package repository;

import java.util.List;

/**
 * @author pdminh
 *
 * @param <T>
 */
public interface IGenericRepository<T> {
	Class<T> getType();

	T findById(String id);

	T save(T bean);

	List<T> saveAll(List<T> objs);

	List<T> findAll();

	void delete(T bean);

	void deleteById(String id);

	void deleteAll(List<T> objs);
}
