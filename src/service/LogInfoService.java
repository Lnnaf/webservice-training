package service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LogInfoService {
	public void writeLogTimeOut(String content) {
		try {
			FileWriter filename = new FileWriter(
					"C:\\Users\\nvlinh\\Downloads\\Ivy\\workspace\\WsTraining\\webContent\\log\\logTimeOut", true);
			BufferedWriter writer = new BufferedWriter(filename);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			writer.write(dtf.format(now) + ": " + content);
			writer.newLine();
			writer.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}
}
