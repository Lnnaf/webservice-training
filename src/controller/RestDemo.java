package controller;

import java.util.List;
import java.util.UUID;

import javax.annotation.security.PermitAll;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import entity.WebServiceDataEntity;
import repository.WebServiceDataRepository;

@Path("information")
@PermitAll
public class RestDemo {
	private WebServiceDataEntity result = new WebServiceDataEntity();
	private WebServiceDataRepository repo = new WebServiceDataRepository();

	@GET
	@Path("/{uuid}")
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response get(@PathParam("uuid") String uuid) {
		try {
			result = repo.findByUuid(uuid);
			if (result != null) {
				return Response.status(Status.OK).entity(result).build();
			} else {
				String msgErr = "Not found any thing with uuid = " + uuid;
				return Response.status(Status.NOT_FOUND).entity(msgErr).build();
			}
		} catch (Exception e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}

	@GET()
	@Produces(MediaType.APPLICATION_JSON)
	public String hello() {
		String sayHelloWorld = "123123123";
		return sayHelloWorld;
	}

	@GET
	@Path("/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		try {
			List<WebServiceDataEntity> results = repo.findAll();
			if (results.isEmpty()) {
				return Response.status(Status.NO_CONTENT).entity(results).build();
			} else {
				return Response.status(Status.OK).entity(results).build();
			}
		} catch (Exception e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	@DELETE
	@Path("/delete/{uuid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteByUuid(@PathParam("uuid") String uuid) {
		try {
			repo.deleteByUuid(uuid);
		} catch (Exception e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.status(Status.OK).build();
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response add(WebServiceDataEntity obj ) {
		obj.setUuid(UUID.randomUUID().toString());
		try {
			repo.save(obj);
		} catch (Exception e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		return Response.status(Status.OK).entity(obj.getUuid()).build();
	}
}
